import React from 'react';
import 'ionicons/dist/css/ionicons.css';
import {Link} from 'react-router-dom';

function TodoItem({id, title, date, completed = false, active = false, number, onClickUp, onClickDown, onClickTop, onClickBottom, onClickToggle, onClickItem})
{
    let className = completed ? "task task-completed" : "task";
    return (
	    <li key={id} className={className} data-clicked-id={id} onClick={onClickItem}>
	    <div className="task-line row-break-670">
	    <div className="text">{number}{number && "."} {title}</div>
	    <div className="date">{date}</div>
	    </div>
	    <div className={active ? "task-line task-menu-shown" : "task-line task-menu-hidden"}>
	    <button className="menu-button" onClick={onClickUp}><ArrowUp /></button>
	    <button className="menu-button" onClick={onClickDown}><ArrowDown /></button>
	    <button className="menu-button" onClick={onClickTop}><ArrowTop /></button>
	    <button className="menu-button" onClick={onClickBottom}><ArrowBottom /></button>
	    <button className="menu-button" onClick={onClickToggle}> {completed ? (<UncompleteIcon />) : (<CompleteIcon />)}</button>
	    <Link to={`/${id}`}><button className="menu-button"><DetailsIcon /></button></Link>
	    </div>
	    </li>
    );
}


function ArrowUp()
{
    return (<i className="ion-md-arrow-round-up todoIcon"></i>);
}

function ArrowDown()
{
    return (<i className="ion-md-arrow-round-down todoIcon"></i>);
}

function ArrowTop()
{
    return (<i className="ion-md-arrow-dropup-circle todoIcon"></i>);
}


function ArrowBottom()
{
    return (<i className="ion-md-arrow-dropdown-circle todoIcon"></i>);
}

function CompleteIcon()
{
    return (<i className="ion-md-checkmark-circle todoIcon"></i>);
}

function UncompleteIcon()
{
    return (<i className="ion-md-checkmark-circle-outline todoIcon"></i>);
}

function DetailsIcon()
{
    return (<i className="ion-md-clipboard todoIcon"></i>);
}

export default TodoItem;
