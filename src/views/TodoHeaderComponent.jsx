import React from 'react';

function TodoHeader({showAllCallback, showCompletedCallback, showActiveCallback, moveToBottomCallback, addCallback, inputErr, inputRemaining, inputUpdateCallback, clearAllCallback, clearCompletedCallback, inputRef})
{
    return (
	<>
	  <div className="row row-break-500">
	    <h1 className="title">My To-Do List</h1>
	    <div>
	      <button className="clear-button" onClick={clearCompletedCallback}>clear completed</button>
	      <button className="clear-button" onClick={clearAllCallback}>clear all</button>
	    </div>
	  </div>
	  <div className="row row-break-380">
	    <input id="js-input" ref={inputRef} data-test="true" className="input" placeholder="Add new task here" onChange={inputUpdateCallback}/>
	    <button className="add-button" onClick={addCallback}>add</button>
	    </div>
	    <div className="row row-break-500">
	    <p>{inputRemaining} characters left</p>
	     <p>{inputErr}</p>
	    </div>
	  <div className="row row-break-670">
	    <button className="add-button" onClick={showAllCallback}>show all</button>
	    <button className="add-button" onClick={showCompletedCallback}>show completed</button>
	    <button className="add-button" onClick={showActiveCallback}>show active</button>
	    <button className="add-button" onClick={moveToBottomCallback}>move finished to bottom</button>
	  </div>
	</>
    );
}

export default TodoHeader;
