export function withClickedItem(event, todos, f)
{
    let target = event.currentTarget;
    //find element that contains clickedId data attribute
    while(target && target.dataset && !target.dataset.clickedId && target.parentElement)
    {
	target = target.parentElement;
    }
    if(!target)
    {
	console.warn("withItem aborted because it has no target");
	return;
    }
    
    const {clickedId} = target.dataset;
    withItem(clickedId, todos, f);
}

export function withItem(id, todos, f)
{
    const index = todos.findIndex(element => element.id === id);

    if(index === -1)
    {
	console.warn("withItem aborted because element was not found");
	return;
    }
    
    return f(index, todos);
}

export function findItem(id, todos)
{
    const index = withItem(id, todos, (index) => (index));
    return index;
}
