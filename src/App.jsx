import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './css/App.css';
import uuid from 'uuid/v1';
import {ListView, DetailsView} from './views';

class App extends Component
{
    state = {
	todos: [],
    }
    
    componentDidMount()
    {
	fetch('https://jsonplaceholder.typicode.com/todos/')
	    .then(response => response.json())
	    .then(json => json.map((e) => {e.id = uuid(); return e;}))
	    .then(json => this.setState({todos: json}))
	    .catch(error => console.log(error));
    }

    setTodo = (todos) => {
	this.setState({todos});
    }


    renderComponent = (Component) => (props) => <Component {...props} todos={this.state.todos}  setTodo={this.setTodo} />
    
    render()
    {
	return( <BrowserRouter>
		<Route
		  path="/"
		  render={this.renderComponent(ListView)} />
		<Route
		  path="/:id"
		  render={this.renderComponent(DetailsView)} />
		</BrowserRouter>);
    }
}   

export default App;


