import React, {Component } from 'react';
import TodoHeader from './TodoHeaderComponent';
import TodoItem from './TodoItemComponent';
import uuid from 'uuid/v1';
import {withClickedItem} from '../helpers';

export class ListView extends Component
{
    maxInput = 100;
    textInput = React.createRef();
    setTodos = this.props.setTodo;
    
    state = {
	active: null,
	filterCompleted: 0,
	inputErr: null,
	inputRemaining: this.maxInput
    }

    swapTask = (event, offset, isValid) => withClickedItem(event,
							   this.props.todos,
							   (index, todos) =>
							   {
							       if(!isValid(index)) return;
							       
							       const temp = todos[index];
							       todos[index] = todos[index+offset];
							       todos[index+offset] = temp;
							       
							       this.setTodos(todos);

							       event.stopPropagation();
							   });

    taskUp = (event) => this.swapTask(event, -1,(index) => (index !== 0));
    taskDown = (event) => this.swapTask(event, 1, (index) => (index !== this.props.todos.length-1));

    moveTask = (event, insertFun) => withClickedItem(event,
						     this.props.todos,
						     (index, todos) =>
						     {
							 const obj = todos[index];
							 todos.splice(index, 1);
							 insertFun.call(todos, obj);
							 this.setTodos(todos);
							 
							 event.stopPropagation();
						     });

    taskTop = (event) => this.moveTask(event, Array.prototype.unshift);
    taskBottom = (event) => this.moveTask(event, Array.prototype.push);

    taskToggleCompletion = (event) => withClickedItem(event,
						      this.props.todos,
						      (index, todos) =>
						      {	
							  todos[index].completed = !todos[index].completed;
							  this.setTodos(todos);
						      });

    taskOnClick = (event) => withClickedItem(event,
					     this.props.todos,
					     (index, todos) =>
					     {
						 this.setState({active:todos[index].id});
						 event.stopPropagation();
					     });

    wrapperOnClick = () => this.setState({active: null});


    sortTodos = (f) => {
	const {todos} = this.props;
	todos.sort(f);
	this.setTodos(todos);
    }
    
    separateTodos = () => {
	this.sortTodos((e1, e2) => {
	    switch(true)
	    {
		case(e1.completed && !e2.completed):
		return 1;
		case(!e1.completed && e2.completed):
		return -1;
		default:
		return 0;
	    }
	});
    }
    
    formatDate = (dateObj) => (`${dateObj.getDate()}.${dateObj.getMonth()+1}.${dateObj.getFullYear()} ${this.datePad(dateObj.getHours())}:${this.datePad(dateObj.getMinutes())}`)

    datePad = (num) => num < 10 ? "0"+num : num;
    
    addNewTodo = (event) => {
	const inputField = this.textInput.current;
	if(inputField.value === "")
	{
	    this.setState({
		inputErr: "you cannot insert empty todo"
	    });
	    return;
	}
	
	if(inputField.value.length > this.maxInput)
	{
	    this.setState({
		inputErr: "item is too long"
	    });
	    return;
	}
	
	const {todos} = this.props;
	todos.unshift({id:uuid(),
		       title: inputField.value,
		       completed: false,
		       date: this.formatDate(new Date())
		      });
	this.setTodos(todos);
	inputField.value = "";
	this.updateInput();
    }

    timer = null;
    updateInput = () =>
    {
	if(!this.timer)
	{
	    const inputField = this.textInput.current;
	    this.setState({
		inputErr: null,
		inputRemaining: this.maxInput-inputField.value.length
	    });
	    this.timer = setTimeout(() => {this.timer = null;}, 500);
	}
    }

    clearAll = () => {
	this.setTodos([]);
	this.setState({
	    active: null
	});
    }
    
    clearCompleted = () => {
	const {todos} = this.props;
	this.setTodos(todos.filter((x) => ( !x.completed)));
    }
    

    filterTasks = (val) => this.setState({filterCompleted: val});
    
    showAll = () => this.filterTasks(0);
    showActive = () => this.filterTasks(1);
    showCompleted = () => this.filterTasks(-1);
    
    renderTodo = ({id, title, completed, date}, index) => {
	if((this.state.filterCompleted === 1 && completed) ||
	   (this.state.filterCompleted === -1 && !completed))
	{
	    return false;
	}

	return (<TodoItem
		number={index+1}
		completed={completed}
		key={id}
		id={id}
		title={title}
		onClickItem={this.taskOnClick}
		date={date}
		active={this.state.active === id}
		onClickToggle={this.taskToggleCompletion}
		onClickUp={this.taskUp}
		onClickDown={this.taskDown}
		onClickTop={this.taskTop}
		onClickBottom={this.taskBottom}
		/>);
	
    };
    
    render() {
	return (
		<div className="wrapper" onClick={this.wrapperOnClick}>
		<TodoHeader
	          showAllCallback={this.showAll}
	          showCompletedCallback={this.showCompleted}
	          showActiveCallback={this.showActive}
	          moveToBottomCallback={this.separateTodos}
	          addCallback={this.addNewTodo}
	          inputRemaining={this.state.inputRemaining}
	          inputErr={this.state.inputErr}
	          inputUpdateCallback={this.updateInput}
	          clearAllCallback={this.clearAll}
	          clearCompletedCallback={this.clearCompleted}
	          inputRef={this.textInput}
		/>
	      <div className="row">
		<ul id="js-list" className="tasklist">
		  {this.props.todos.map(this.renderTodo)}
		</ul>
	      </div>
	    </div>
	);
    }
}
