import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import {findItem} from '../helpers';

export class DetailsView extends Component
{
    setTodos = this.props.setTodo;
    
    state = {
	index: -1,
	item: undefined,
    }

    componentDidMount()
    {
	const {todos} = this.props;
	const {id} = this.props.match.params;
	
	const index = findItem(id, todos);
	const item = todos[index];
	
	if(index === undefined)
	    this.props.history.replace('/');
	
	this.setState({index, item});
    }

    componentWillUnmount()
    {
	const {todos} = this.props;
	const {index} = this.state;

	if(!todos[index])
	    return;
	
	this.setTodos(todos);
    }
    
    changeDescription = (event) =>
    {
	const description = event.currentTarget.value;
	const {item} = this.state;
	item.description = description;
	this.setState({item});
    }
    
    render()
    {
	return(
	    <Modal>
		<div className="row">
		    <div className="modalTitle">{this.state.item && this.state.item.title}</div>
		    <Link className="modalClose" to="/" >X</Link>
		</div>
		<div className="row">
		    <textarea className="modalTextarea" onChange={this.changeDescription} value={this.state.item && this.state.item.description}></textarea>
		</div>
	    </Modal>
	);
    }
}

function Modal(props)
{
    return(
	<div className="modalBackground">
	    <div className="modalWindow">
	        {props.children}
	    </div>
	</div>
    );
}


export default DetailsView;
